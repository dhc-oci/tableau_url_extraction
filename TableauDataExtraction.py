import hashlib as hashlib
import psycopg2 as pg
import cx_Oracle as oraConnection
import datetime
import DbConnection as db
# connstr =db.oracledb['connstr']
user = db.oracledb['username']
passwd = db.oracledb['password']
dsn = db.oracledb['dsn']

def hasKey(string_name):
    return hashlib.sha256(string_name.encode()).hexdigest()

def GetAllRow():
    rows=None
    pgconn = pg.connect(host=db.postgresdb['host'], port=db.postgresdb['port'], database=db.postgresdb['database'], user=db.postgresdb['user'], password=db.postgresdb['password'])
    cur=pgconn.cursor()
    # DataSource from Oracle
    SQLString=" SELECT v.id worksheet_id,v.workbook_id,v.view_url workbook_worksheet_name,v.created_at Worksheet_Created,w.created_at workbook_Created," \
              " updated_at workbook_Updated,UPPER(s.Name) Site_Name " \
              " FROM _views v INNER JOIN  _workbooks w on v.workbook_id=w.id  " \
              " INNER JOIN _sites s on s.id=w.site_id AND Upper(s.Name) in ('PRODUCTION','LEADERSHIP')"
    cur.execute(SQLString)
    rows=cur.fetchall()
    cur.close()
    pgconn.close()
    return rows

def InsertOracle(RowSize, rows):
    try:
        OracleList = []
        for row in rows:
            worksheet_id = row[0]  # worksheet_id
            workbook_id = row[1]  # workbook_id
            worksheet_workbook_name = row[2]  # worksheet_workbook_name
            workbook_name, workSheet_name = row[2].split("/")  # workbook_name, worksheet_name
            worksheet_created = row[3]  # worksheet_created
            workbook_created = row[4]  # workbook_created
            workbook_updated = row[5]  # workbook_updated
            site_name = row[6]  # workbook_updated
            load_date = datetime.datetime.now()  # load_date
            OracleList.append((worksheet_id, workbook_id, worksheet_workbook_name, workSheet_name, workbook_name,
                               worksheet_created, workbook_created, workbook_updated, site_name, load_date))
        # conn = oraConnection.connect(connstr)
        conn = oraConnection.connect(user=user, password=passwd, dsn=dsn)
        curs = conn.cursor()
        curs.executemany(
            " INSERT INTO HUB.MOD_TABLEAU_LINKS_STAGE(WORKSHEET_ID,WORKBOOK_ID,WORKBOOK_WORKSHEET_NAME,WORKSHEET_NAME,WORKBOOK_NAME,WORKSHEET_CREATED,WORKBOOK_CREATED,"
            " WORKBOOK_UPDATED,SITE_NAME,LOAD_DATE) "
            " VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10)", OracleList)
        conn.commit()
    except Exception as e:
        WriteLogInfo("ModLog.txt", "Load Error {}".format(e.message) + str(datetime.datetime.today())+"\n")


# This is filtered out only the MOD only.
def Validate_MODTableau():
    SQLString=" DELETE FROM HUB.Mod_Tableau_Links_stage a WHERE  a.workbook_worksheet_name not in (SELECT workbook_worksheet_name FROM mod_tableau_links) "
    # conn = oraConnection.connect(connstr)
    conn = oraConnection.connect(user=user, password=passwd, dsn=dsn)
    curs = conn.cursor()
    curs.execute(SQLString)
    conn.commit()
    curs.close()


def TableRowCountOracle(Table_Name):
    # conn = oraConnection.connect(connstr)
    conn = oraConnection.connect(user=user, password=passwd, dsn=dsn)
    curs = conn.cursor()
    sql_statement= " SELECT COUNT(*) FROM "+ Table_Name
    curs.execute(sql_statement)
    row_count =curs.fetchone()
    curs.close()
    conn.close()
    print("TableRowCountOracle row count:",row_count)
    return row_count

def TruncateOracleTable(Table_Name):
    # conn = oraConnection.connect(connstr)
    conn = oraConnection.connect(user=user, password=passwd, dsn=dsn)
    curs = conn.cursor()
    # sql_Statement=" TRUNCATE TABLE " + Table_Name
    sql_Statement="DELETE FROM " + Table_Name
    curs.execute(sql_Statement)
    curs.execute("COMMIT")
    curs.close()
    conn.close()

def WriteLogInfo(file_name, content):
    sysdate=str(datetime.datetime.now())
    with open(file_name,'a+') as f:
        file_content=sysdate +"\t"+content+"\n"
        f.write(file_content)

if __name__=="__main__":
    return_row=GetAllRow()
    if return_row is not None:
        TruncateOracleTable("HUB.MOD_TABLEAU_LINKS_STAGE")
        row_size, rows = len(return_row), return_row
        InsertOracle(row_size,rows)
        LoadedRow=TableRowCountOracle("HUB.Mod_Tableau_Links_stage")
        WriteLogInfo("ModLog.txt", str(LoadedRow)+" record(s) loaded at " + str(datetime.datetime.today()))
